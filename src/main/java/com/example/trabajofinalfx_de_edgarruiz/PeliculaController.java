package com.example.trabajofinalfx_de_edgarruiz;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PeliculaController implements Initializable {

    @FXML
    public ImageView peli1, peli2, peli3, peli4, peli5, peli6, ivLogo;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Image image1 = new Image("file:Fotos/Films/InfinityWar.jpg");
        Image image2 = new Image("file:Fotos/Films/StarWarsRougeOne.jpg");
        Image image3 = new Image("file:Fotos/Films/HTTYD2.jpg");
        Image image4 = new Image("file:Fotos/Films/Bumblebee.jpg");
        Image image5 = new Image("file:Fotos/Films/JusticeLeague.jpeg");
        Image image6 = new Image("file:Fotos/Films/Akira.jpg");
        Image imageLogo = new Image("file:Fotos/Others/cinema-division.jpg");
        peli1.setImage(image1);
        peli2.setImage(image2);
        peli3.setImage(image3);
        peli4.setImage(image4);
        peli5.setImage(image5);
        peli6.setImage(image6);
        ivLogo.setImage(imageLogo);

    }

    @FXML
    void onClick(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader fxmlLoader  = new FXMLLoader(getClass().getResource("personal_information.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("");
        stage.show();

    }

}