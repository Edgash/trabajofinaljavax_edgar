package com.example.trabajofinalfx_de_edgarruiz;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.chrono.ChronoLocalDate;
import java.util.ResourceBundle;

public class PersonalInfoController implements Initializable {

    @FXML
    private ImageView ivLogo;

    @FXML
    private DatePicker dpFecha;

    @FXML
    private Button btSum, btRest, btSiguiente;

    @FXML
    private Label lblCantidad, lblPrecio;

    @FXML
    private TextField tfApellido, tfCCV, tfEmail, tfNombre, tfNumero, tfTitular, tfcaducidad;

    @FXML
    private ComboBox<String> cbHour;

    private ObservableList<String> olHour;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Image image = new Image("file:Fotos/Others/cinema-division.jpg");
        ivLogo.setImage(image);

        buttonsCircles();
        comboBoxes();
        blockDate();
    }

    private void blockDate() {
        dpFecha.setValue(LocalDate.of(2021, Month.DECEMBER,25));

        dpFecha.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate december25 = LocalDate.of(2021, 12, 25);
                LocalDate december31 = LocalDate.of(2021, 12, 31);


                setDisable(date.compareTo(ChronoLocalDate.from(december25)) < 0 || date.compareTo(ChronoLocalDate.from(december31)) > 0);
            }
        });
    }

    private void buttonsCircles() {
        int r = 50;
        btRest.setShape(new Circle(r));
        btRest.setMinSize(r, r);
        btRest.setMaxSize(r, r);
        btSum.setShape(new Circle(r));
        btSum.setMinSize(r, r);
        btSum.setMaxSize(r, r);

    }

    private void comboBoxes() {

        olHour = FXCollections.observableArrayList("17:45", "18:30", "20:00");
        cbHour.setItems(olHour);

    }

    @FXML
    void btnCantidad(ActionEvent event) {
        String opcion = ((Button) event.getSource()).getText();

        String textoLabel = lblCantidad.getText();
        int numLabel = Integer.parseInt(textoLabel);
        String precioLabel = lblPrecio.getText();
        int numPrecio = Integer.parseInt(precioLabel);

        switch (opcion) {
            case "-":
                if (numLabel >= 1) {
                    numLabel = numLabel - 1;
                    lblCantidad.setText(String.valueOf(numLabel));
                }
                if (numLabel == 0) {
                    lblPrecio.setText("0");
                }else{
                    LocalDate day = dpFecha.getValue();
                    day.getDayOfWeek();

                    if(day.getDayOfWeek() == DayOfWeek.WEDNESDAY){
                        lblPrecio.setText("5");
                        numPrecio = numPrecio - 5;
                        lblPrecio.setText(String.valueOf(numPrecio));
                    }else{
                        lblPrecio.setText("6");
                        numPrecio = numPrecio - 6;
                        lblPrecio.setText(String.valueOf(numPrecio));
                    }
                }
                break;
            case "+":
                numLabel = numLabel + 1;
                lblCantidad.setText(String.valueOf(numLabel));

                LocalDate day = dpFecha.getValue();
                day.getDayOfWeek();

                if(day.getDayOfWeek() == DayOfWeek.WEDNESDAY){
                    lblPrecio.setText("5");
                    numPrecio = numPrecio + 5;
                    lblPrecio.setText(String.valueOf(numPrecio));
                }else{
                    lblPrecio.setText("6");
                    numPrecio = numPrecio + 6;
                    lblPrecio.setText(String.valueOf(numPrecio));
                }
                break;
        }
    }


    @FXML
    void btnSiguiente(ActionEvent event) throws IOException {
        String cantidadString = lblCantidad.getText();
        int numPrecio = Integer.parseInt(cantidadString);

        if(numPrecio == 0){
            String mensaje = "Debes pedir al menos 1 entrada.";
            Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje, ButtonType.APPLY);
            alert.setTitle("Error!");
            alert.setHeaderText(mensaje);
            alert.setContentText("");
            alert.showAndWait();
        }else if(tfApellido.getText().isEmpty() || tfCCV.getText().isEmpty() || tfEmail.getText().isEmpty() ||
                tfNombre.getText().isEmpty() || tfNumero.getText().isEmpty() || tfTitular.getText().isEmpty() ||
                tfcaducidad.getText().isEmpty() || cbHour.getSelectionModel().isEmpty() ||
                dpFecha.getValue() == null) {
            String mensaje = "Algún campo está vacío.";
            Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje, ButtonType.APPLY);
            alert.setTitle("Error!");
            alert.setHeaderText(mensaje);
            alert.setContentText("Rellene toda la información.");
            alert.showAndWait();
        }else{
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("sala.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("");
            stage.show();
        }
    }

}

