package com.example.trabajofinalfx_de_edgarruiz;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SalaController implements Initializable {

    @FXML
    private ImageView ivLogo;

    @FXML
    private Button btTerminar;

    @FXML
    private GridPane gpCine;

    @FXML
    private ComboBox<String> cbEntradas;

    private ObservableList<String> olEntradas;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Image image = new Image("file:Fotos/Others/cinema-division.jpg");
        ivLogo.setImage(image);

        olEntradas = FXCollections.observableArrayList("Comprar", "Reservar");
        cbEntradas.setItems(olEntradas);

        
    }

    @FXML
    void btnTerminar(ActionEvent event) throws IOException {

        if(cbEntradas.getSelectionModel().isEmpty()){
            String mensaje = "Algún campo está vacío.";
            Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje, ButtonType.APPLY);
            alert.setTitle("Error!");
            alert.setHeaderText(mensaje);
            alert.setContentText("Rellene toda la información.");
            alert.showAndWait();
        }else{
            String mensaje = "Gracias por su compra.";
            Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje, ButtonType.APPLY);
            alert.setTitle("Gracias!");
            alert.setHeaderText(mensaje);
            alert.setContentText("Disfrute de la película.");
            alert.showAndWait();
        }
    }

}
