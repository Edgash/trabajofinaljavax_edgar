module com.example.trabajofinalfx_de_edgarruiz {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.trabajofinalfx_de_edgarruiz to javafx.fxml;
    exports com.example.trabajofinalfx_de_edgarruiz;
}